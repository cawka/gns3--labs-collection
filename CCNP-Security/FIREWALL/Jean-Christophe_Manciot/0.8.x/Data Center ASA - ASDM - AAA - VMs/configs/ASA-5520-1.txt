ASA Version 8.4(2)
!
hostname ASA-5520-1
domain-name Data-Center-ASAs.net
enable password NTHthE8iFuXcYbG. encrypted
passwd PDoHO2U4zt11THgf encrypted
names
dns-guard
!
interface GigabitEthernet0
 nameif management
 security-level 100
 ip address 192.168.127.254 255.255.255.0
!
interface GigabitEthernet1
 nameif DMZ-1
 security-level 50
 ip address 192.168.11.254 255.255.255.0
!
interface GigabitEthernet2
 nameif DMZ-2
 security-level 50
 ip address 192.168.21.254 255.255.255.0
!
interface GigabitEthernet3
 nameif ASAs
 security-level 50
 ip address 192.168.120.254 255.255.255.0
!
interface GigabitEthernet4
 nameif outside
 security-level 0
 ip address dhcp setroute
!
interface GigabitEthernet5
 nameif Inside
 security-level 100
 ip address 192.168.56.254 255.255.255.0
!
ftp mode passive
clock timezone CEST 1
clock summer-time CEDT recurring last Sun Mar 2:00 last Sun Oct 3:00
dns domain-lookup management
dns domain-lookup DMZ-1
dns domain-lookup DMZ-2
dns domain-lookup Inside
dns server-group DefaultDNS
 name-server 192.168.1.201
 name-server 8.8.8.8
 domain-name Data-Center-ASAs.net
same-security-traffic permit inter-interface
same-security-traffic permit intra-interface
object network DMZ-VLAN-2
 subnet 192.168.2.0 255.255.255.0
object network VMs-Ubuntu
 subnet 192.168.56.0 255.255.255.0
object network ASAs-Interface
 subnet 192.168.120.0 255.255.255.0
object network DMZ-1-Interface
 subnet 192.168.11.0 255.255.255.0
object network DMZ-2-Interface
 subnet 192.168.21.0 255.255.255.0
object network DMZ-VLAN-3
 subnet 192.168.3.0 255.255.255.0
object network DMZ-VLAN-4
 subnet 192.168.4.0 255.255.255.0
object network DMZ-VLAN-5
 subnet 192.168.5.0 255.255.255.0
object network Management-Interface
 subnet 192.168.127.0 255.255.255.0
object network SMTP-Server
 host 192.168.2.1
object-group network DMZ-VLANs
 network-object object DMZ-VLAN-2
 network-object object DMZ-VLAN-3
 network-object object DMZ-VLAN-4
 network-object object DMZ-VLAN-5
access-list EtherChannel-1_access_out extended permit icmp any any
access-list EtherChannel-1_access_out extended permit ip any any
access-list DMZ-1_access_in extended permit icmp any any
access-list DMZ-1_access_in extended permit ip any any
access-list DMZ-1_access_out extended permit icmp any any
access-list DMZ-1_access_out extended permit ip any any
access-list EtherChannel-1_access_in extended permit icmp any any
access-list EtherChannel-1_access_in extended permit ip any any
access-list Inside_access_in extended permit icmp any any
access-list Inside_access_in extended permit ip any any
access-list Inside_access_out extended permit icmp any any
access-list Inside_access_out extended permit ip any any
access-list DMZ-2_access_in extended permit icmp any any
access-list DMZ-2_access_in extended permit ip any any
access-list DMZ-2_access_out extended permit icmp any any
access-list DMZ-2_access_out extended permit ip any any
access-list ASAs_access_in extended permit ip host 192.168.120.253 any
access-list management_access_in extended permit ip 192.168.127.0 255.255.255.0 any
access-list outside_access_in extended permit icmp any any time-exceeded
access-list outside_access_in extended permit icmp any any unreachable
access-list outside_access_in extended deny ip any any log alerts
pager lines 24
logging enable
logging timestamp
logging buffer-size 10000
logging asdm-buffer-size 512
logging console errors
logging monitor errors
logging buffered errors
logging history errors
logging asdm warnings
logging mail critical
logging from-address ASA-5520-1@Data-Center-ASAs.net
logging recipient-address manciot.jeanchristophe@gmail.com level alerts
logging flash-bufferwrap
logging message 711004 level notifications
mtu management 1500
mtu DMZ-1 1500
mtu DMZ-2 1500
mtu ASAs 1500
mtu outside 1500
mtu Inside 1500
ipv6 access-list outside_access_ipv6_in deny ip any any
no failover
icmp unreachable rate-limit 1 burst-size 1
asdm image disk0:/asdm-647.bin
no asdm history enable
arp timeout 14400
!
object network DMZ-VLAN-2
 nat (any,outside) dynamic interface
object network VMs-Ubuntu
 nat (any,outside) dynamic interface
object network ASAs-Interface
 nat (any,outside) dynamic interface
object network DMZ-1-Interface
 nat (any,outside) dynamic interface
object network DMZ-2-Interface
 nat (any,outside) dynamic interface
object network DMZ-VLAN-3
 nat (any,outside) dynamic interface
object network DMZ-VLAN-4
 nat (any,outside) dynamic interface
object network DMZ-VLAN-5
 nat (any,outside) dynamic interface
object network Management-Interface
 nat (any,outside) dynamic interface
access-group management_access_in in interface management
access-group DMZ-1_access_in in interface DMZ-1
access-group DMZ-1_access_out out interface DMZ-1
access-group DMZ-2_access_in in interface DMZ-2
access-group DMZ-2_access_out out interface DMZ-2
access-group ASAs_access_in in interface ASAs
access-group outside_access_in in interface outside
access-group outside_access_ipv6_in in interface outside
access-group Inside_access_in in interface Inside
access-group Inside_access_out out interface Inside
!
router ospf 1
 network 192.168.11.0 255.255.255.0 area 0
 network 192.168.21.0 255.255.255.0 area 0
 network 192.168.56.0 255.255.255.0 area 0
 network 192.168.120.0 255.255.255.0 area 0
 network 192.168.127.0 255.255.255.0 area 0
 area 0
 timers spf 1 5
 timers lsa-group-pacing 10
 log-adj-changes
 redistribute connected subnets
 default-information originate metric 1
!
timeout xlate 3:00:00
timeout conn 1:00:00 half-closed 0:10:00 udp 0:02:00 icmp 0:00:02
timeout sunrpc 0:10:00 h323 0:05:00 h225 1:00:00 mgcp 0:05:00 mgcp-pat 0:05:00
timeout sip 0:30:00 sip_media 0:02:00 sip-invite 0:03:00 sip-disconnect 0:02:00
timeout sip-provisional-media 0:02:00 uauth 0:05:00 absolute
timeout tcp-proxy-reassembly 0:01:00
timeout floating-conn 0:00:00
dynamic-access-policy-record DfltAccessPolicy
user-identity default-domain LOCAL
aaa authentication ssh console LOCAL
aaa authentication enable console LOCAL
aaa authentication http console LOCAL
aaa authorization command LOCAL
aaa local authentication attempts max-fail 3
aaa authorization exec LOCAL
http server enable
http 192.168.0.0 255.255.0.0 management
snmp-server host Inside 192.168.56.251 community ***** version 2c
no snmp-server location
no snmp-server contact
snmp-server community *****
snmp-server enable traps snmp authentication linkup linkdown coldstart warmstart
snmp-server enable traps syslog
snmp-server enable traps cpu threshold rising
crypto ca trustpoint ASA-5520-1-Self-Signed-Certificate
 enrollment self
 subject-name CN=ASA-5520-1.Data-Center-ASAs
 serial-number
 ip-address 192.168.137.254
 keypair RSA-For-Self-Signed-Certificate
 proxy-ldc-issuer
 crl configure
crypto ca certificate chain ASA-5520-1-Self-Signed-Certificate
 certificate f2e54151
    308202e6 3082024f a0030201 020204f2 e5415130 0d06092a 864886f7 0d010105
    05003081 84312430 22060355 0403131b 4153412d 35353230 2d312e44 6174612d
    43656e74 65722d41 53417331 5c301206 03550405 130b3132 33343536 37383941
    42301c06 092a8648 86f70d01 0908130f 3139322e 3136382e 3133372e 32353430
    2806092a 864886f7 0d010902 161b4153 412d3535 32302d31 2e446174 612d4365
    6e746572 2d415341 73301e17 0d313330 33313431 35303532 365a170d 32333033
    31323135 30353236 5a308184 31243022 06035504 03131b41 53412d35 3532302d
    312e4461 74612d43 656e7465 722d4153 4173315c 30120603 55040513 0b313233
    34353637 38394142 301c0609 2a864886 f70d0109 08130f31 39322e31 36382e31
    33372e32 35343028 06092a86 4886f70d 01090216 1b415341 2d353532 302d312e
    44617461 2d43656e 7465722d 41534173 30819f30 0d06092a 864886f7 0d010101
    05000381 8d003081 89028181 00bf49bf da12f026 6fd716d5 3121003b 13829f0d
    d49b4b61 e250ec60 bfaeed48 91df9baf e7f4dc94 9bc4e9f6 852d4f3d c4d46ade
    78a447c3 7032a030 97618e4d 03a8503e 5d79281c 3d6351d2 09a8ede4 c1467739
    59d65657 e3d1f100 0b437837 16db4781 e1f762db cdb0c56a e95caa53 83bdb4a5
    8069bd0d e596e5cb 3db86ad2 19020301 0001a363 3061300f 0603551d 130101ff
    04053003 0101ff30 0e060355 1d0f0101 ff040403 02018630 1f060355 1d230418
    30168014 30ab5d09 a43de100 8def0c5f 18801a02 45716bec 301d0603 551d0e04
    16041430 ab5d09a4 3de1008d ef0c5f18 801a0245 716bec30 0d06092a 864886f7
    0d010105 05000381 81007079 889a0f8c d92a1ce6 7c08cb03 6838654c bca54ff6
    d51ddbd9 9b7c9ec5 4753aea8 e039f64a 23acaee5 8041c52a c7b8e5c5 c3b9c250
    ae04f3f9 5a27470f 62a9b0bc a76fc813 03c417e0 832283d9 0bb9dc3e ea93f4c2
    10ea9048 2d54a5d6 bbcad34c e23d00ad fedb30ef 5156d734 709de7eb 130d7e06
    8bbac22e 6063d822 c6f4
  quit
telnet timeout 5
ssh 192.168.0.0 255.255.0.0 Inside
ssh timeout 5
console timeout 0
management-access management
dhcpd dns 192.168.1.201 8.8.8.8
dhcpd lease 86400
dhcpd ping_timeout 100
dhcpd domain Inside
dhcpd update dns both
!
dhcpd update dns both interface Inside
!
dhcprelay server 192.168.11.201 DMZ-1
dhcprelay server 192.168.21.202 DMZ-2
dhcprelay enable Inside
dhcprelay setroute Inside
dhcprelay timeout 60
threat-detection basic-threat
threat-detection scanning-threat
threat-detection statistics
threat-detection statistics tcp-intercept rate-interval 30 burst-rate 400 average-rate 200
ntp server 192.43.244.18 prefer
ntp server 83.212.108.67 prefer
ntp server 192.93.2.20 prefer
ssl trust-point ASA-5520-1-Self-Signed-Certificate management
ssl trust-point ASA-5520-1-Self-Signed-Certificate Inside
webvpn
username actionmystique password z0qWOjj1NDTOlw/7 encrypted privilege 15
!
class-map global-class
 match default-inspection-traffic
!
!
policy-map global-policy
 class global-class
  inspect icmp
  inspect icmp error
!
service-policy global-policy global
smtp-server 93.17.128.7
privilege cmd level 3 mode exec command perfmon
privilege cmd level 3 mode exec command ping
privilege cmd level 3 mode exec command who
privilege cmd level 3 mode exec command logging
privilege cmd level 3 mode exec command failover
privilege cmd level 3 mode exec command vpn-sessiondb
privilege cmd level 3 mode exec command packet-tracer
privilege show level 5 mode exec command import
privilege show level 5 mode exec command running-config
privilege show level 3 mode exec command reload
privilege show level 3 mode exec command mode
privilege show level 3 mode exec command firewall
privilege show level 3 mode exec command asp
privilege show level 3 mode exec command cpu
privilege show level 3 mode exec command interface
privilege show level 3 mode exec command clock
privilege show level 3 mode exec command dns-hosts
privilege show level 3 mode exec command access-list
privilege show level 3 mode exec command logging
privilege show level 3 mode exec command vlan
privilege show level 3 mode exec command ip
privilege show level 3 mode exec command ipv6
privilege show level 3 mode exec command failover
privilege show level 3 mode exec command asdm
privilege show level 3 mode exec command arp
privilege show level 3 mode exec command route
privilege show level 3 mode exec command ospf
privilege show level 3 mode exec command aaa-server
privilege show level 3 mode exec command aaa
privilege show level 3 mode exec command eigrp
privilege show level 3 mode exec command crypto
privilege show level 3 mode exec command ssh
privilege show level 3 mode exec command vpn-sessiondb
privilege show level 3 mode exec command vpn
privilege show level 3 mode exec command dhcpd
privilege show level 3 mode exec command blocks
privilege show level 3 mode exec command wccp
privilege show level 3 mode exec command dynamic-filter
privilege show level 3 mode exec command webvpn
privilege show level 3 mode exec command service-policy
privilege show level 3 mode exec command uauth
privilege show level 3 mode exec command compression
privilege show level 3 mode configure command interface
privilege show level 3 mode configure command clock
privilege show level 3 mode configure command access-list
privilege show level 3 mode configure command logging
privilege show level 3 mode configure command ip
privilege show level 3 mode configure command failover
privilege show level 5 mode configure command asdm
privilege show level 3 mode configure command arp
privilege show level 3 mode configure command route
privilege show level 3 mode configure command aaa-server
privilege show level 3 mode configure command aaa
privilege show level 3 mode configure command crypto
privilege show level 3 mode configure command ssh
privilege show level 3 mode configure command dhcpd
privilege show level 5 mode configure command privilege
privilege clear level 3 mode exec command dns-hosts
privilege clear level 3 mode exec command logging
privilege clear level 3 mode exec command arp
privilege clear level 3 mode exec command aaa-server
privilege clear level 3 mode exec command crypto
privilege clear level 3 mode exec command dynamic-filter
privilege cmd level 3 mode configure command failover
privilege clear level 3 mode configure command logging
privilege clear level 3 mode configure command arp
privilege clear level 3 mode configure command crypto
privilege clear level 3 mode configure command aaa-server
prompt hostname context
no call-home reporting anonymous
call-home
 profile CiscoTAC-1
  no active
  destination address http https://tools.cisco.com/its/service/oddce/services/DDCEService
  destination address email callhome@cisco.com
  destination transport-method http
  subscribe-to-alert-group diagnostic
  subscribe-to-alert-group environment
  subscribe-to-alert-group inventory periodic monthly
  subscribe-to-alert-group configuration periodic monthly
  subscribe-to-alert-group telemetry periodic daily
hpm topN enable
crashinfo save disable
Cryptochecksum:934c74b9917f017bc2b310d66c106bba
: end