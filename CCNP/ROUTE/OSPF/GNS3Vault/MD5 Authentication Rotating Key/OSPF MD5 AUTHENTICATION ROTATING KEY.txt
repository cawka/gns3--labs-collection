OSPF MD5 AUTHENTICATION ROTATING KEY
WRITTEN BY RENE MOLENAAR ON 20 SEPTEMBER 2011. POSTED IN OSPF
SCENARIO:
You work for the government as a contracted network engineer. They want you to improve their OSPF security. Instead of using a single key for all routers they want to ensure each OSPF neighbor adjacency has a different key. Let's find out if you can lock this one down.
GOAL:
* All IP addresses have been preconfigured for you.
* Configure OSPF on all routers. Achieve full connectivity.
* Router Twist and Turn have to use password "PASSWORD".
* Router Twist and Rotate have to use password "VAULT".
IOS:
c3640-jk9s-mz.124-16.bin

