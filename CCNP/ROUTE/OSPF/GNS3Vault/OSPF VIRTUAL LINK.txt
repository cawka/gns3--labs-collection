OSPF VIRTUAL LINK
WRITTEN BY RENE MOLENAAR ON 30 AUGUST 2011. POSTED IN OSPF
SCENARIO:
You are a freelance network engineer specialized in routing and switching. One of your customers (a local butcher) has some trouble setting up their OSPF network. Some of the networks are not reachable and it's up to you to provide them with a solu
GOAL:
* All IP addresses have been preconfigured for you as specified in the topology picture.
* Each router has a loopback0 interface.
* Configure OSPF on all routers.
* Ensure you restore connectivity for the discontigious backbone area 0.
* Ensure area 2 has connectivity to the backbone area.
IOS:
c3640-jk9s-mz.124-16.bin

